%% Initialisierung Simulink und SimScape Modell des 1/4-PKWs
%
% Autor: Rüdiger Götting
% Lösung DGL: Matthias Graf, Modul Maschinendynamik
% Dateiname: ~Vorlesungen/CAE-Simulation/matlab/Viertel PKW/init.m
% Datum: 24.4.2021, SS2021

g=9.81;
format compact
date

%% Parameter des PKWs
%
mA=300;               % Masse Viertelauto
cA=30000;             % Federsteifigkeit Federung [N/m], bisher 25000N/m
l0A=0.2;              % Vorspannung Feder [m] bisher 0.1m 
mR=30;                % Masse Rad
cR=120000 ;           % Federsteifigkeit Reifen
l0R=0.15;             % Vorspannung Reifen [m] bisher 0.0154m

%% Matrizen für Variante 2 der Lösung
% Matrizen: 
%   Massen
%   Dämpfungen
%   Steifigkeiten
%   Gewichtskräfte
%   Vorspannungen

M=[mA 0
   0, mR]
B0=[1,-1
   -1,1]
C=[cA,-cA
  -cA,cR+cA]
G=[mA*g
   mR*g]
F0=[cA*l0A
    cR*l0R-cA*l0A]

%% Statisches Gleichgewicht (aus Matrizen)
%  Mit diesen Anfangswerten werden die Positionen jeweils
% von der Gleichgewichtslage gemessen.
%  Im Simulink-Modell wird der Anfangswert gesetzt und der
% Gleichgewichtswert abgezogen.
%  Der Spaltenvektor x ist von oben nach unten gezählt:
% oben Aufbau xA und unten Rad xR
x0=C^-1*(F0-G)

%% Kontrolle:
%  xA0= -0.02930 = Position Aufbau aus Simulink abgelesen
%  xR0= -0.01158 = Position Rad aus Simulink abgelesen
% Statisches Gleichgewicht aus algebraischer Lösung
% xA0=l0A + l0R - (cR*mA*g + cA*(mA + mR)*g)/(cA*cR);
% xR0=l0R-g*(mA + mR)/cR;
% x0=[xA0,xR0]'

%% Drei Eingangssignalen der Simulationen
%  Drei mögliche Eingangssignale.
%  Das Standardeingangssignal ist eine Stufe!
%  Für diese Eingangssignale werden die Parameter definiert.
%  
kStep=0;
kChirp=1;
% kKopfsteinpflaster=2;
input_signal=kStep;
%%% Stufe
stufe.hoehe=0.05;
stufe.time= 3;
stufe.b= 4000;
%%% Chirp
sweep.start=0.1;
sweep.end=15;
sweep.time=60;
sweep.amplitude=0.1;
sweep.b= 400;
%%% Kopfsteinpflaster
% v= 50/3.6;
% breite= 20/100;
% kopfstein.frequenz=1/(2*breite/v);
% kopfstein.amplitude= 2.5/100;
% kopfstein.time= 3;
% kopfstein.periode= 2;
% kopfstein.anteil= 50;
% kopfstein.b= 4000;
B=B0*stufe.b;

