%% L�sung Aufgabe "Viertel PKW"
% 
%  Autor: R�diger G�tting, Veranstaltung CAE/Simulation
%  L�sung DGL und Herleitung, Matthias Graf, Vorlesung "Maschinendynamik"
<<<<<<<
%  Datum: 20.4.2021
=======
%  Datum: 24.4.2021, Version SS2021
>>>>>>>
%
% Lit. 
%  Dieter Kraft, "Kompendium der Maschinendynamik", Bericht Fachhochschule M�nchen, 
%  <http://dkraft.userweb.mwn.de/data/mady.pdf>

%% Initialisierung

init;
open_system( "viertel_PKW_sl")

%% Simulation einer Stufe
%  Es wird der Simulink-Block 'Variant Source' benutzt, um 
%  unterschiedliche Eingangssignale w�hlen zu k�nnen.
%  Anschlie�end Start der Simulation

input_signal= kStep;
B=B0*stufe.b;
sim( 'viertel_PKW_sl.slx', stufe.time);

%%% Abbildungen
figure
plot( Schwingung{1}.Values.Time, Schwingung{1}.Values.Data(:,1),...
    '-.g', 'LineWidth', 1.5)
hold on
plot( Schwingung{1}.Values.Time, Schwingung{1}.Values.Data(:,2),...
    '-b', 'LineWidth', 2)
plot( Schwingung{1}.Values.Time, Schwingung{1}.Values.Data(:,3),....
    '-r', 'LineWidth', 2)
str= sprintf( '1/4-Auto, D�mpfing b=%.0fN/(m/sec)', stufe.b);
beautify(str,'Zeit/sec','Amplituden/cm')
legend('Stra�enprofil','Aufbau','Rad')

%% Simulation eines Frequenz-Sweeps
%  * Inputsignal Block "Chirp"
%  * Im Simulink-Modell wird ein Array der Frequenzen erzeugt.
%  Dieser wird dann als x-Werte benutzt.
%  * Damit die Resonanzen gut zu erkennen sind, wird die D�mpfung
% herabgesetzt.

input_signal= kChirp;
B= B0*sweep.b;
sim( 'viertel_PKW_sl.slx', sweep.time);

%%% Abbildungen
frequenz= (sweep.end-sweep.start)/sweep.time.*Schwingung{1}.Values.Time;
figure
tiledlayout('flow')
nexttile
plot( frequenz, Schwingung{1}.Values.Data(:,2),'-r', 'LineWidth', 1.5)
legend('Aufbau')
str= sprintf( '1/4-Auto, Amplitude= %.0fcm, D�mpfing b=%.0fN/(m/sec)', sweep.amplitude*100, sweep.b);
beautify( str,'Frequenz/Hz','Amplituden/cm')
nexttile
plot( frequenz, Schwingung{1}.Values.Data(:,3),'-b', 'LineWidth', 1.5)
axis('auto')
legend('Rad')
beautify( str,'Frequenz/Hz','Amplituden/cm')

%% Eigenwerte und relative D�mpfungen
%  Eigenfrequenzen aus den Matrizen (f�r unged�mpfte Schwingung) und den Simulink Ergebnissen
%  Absch�tzungen der relativen D�mpfungen D
%
%  Die Eigenfrequenzen werden f�r die unged�mpfte Schwingung berechnent.

eig(C*M^-1);
fE=sqrt(ans)/(2*pi);

%%% Eigenfrequenzen aus Diagrammen
% Benutzt wurden die Cursor im Simulink-Scope oder im Sweep-Diagramm
%  Reihenfolge im Array:     [f-Aufbau; f-Rad]
fEx=[0.7095*2; 5.896*2];

% gamma=sweep.b/(2*mR);
% D(2)=gamma/(2*pi*fEx(2));
% gamma=sweep.b/(2*mA);
% D(1)=gamma/(2*pi*fEx(1));

table(fE,fEx, ...
    'VariableNames', {'f_Eigenfrequenzen','f_Simulation'}, ...
    'RowNames', {'Aufbau','Rad'})
