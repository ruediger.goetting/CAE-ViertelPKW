%% Initialisierung Simulink und SimScape Modell des 1/4-PKWs
%
% Autor: R�diger G�tting
% L�sung DGL: Matthias Graf, Vorlesung Maschinendynamik
% Dateiname: ~Vorlesungen/CAE-Simulation/matlab/Federung/initialize.m
% Datum: 21.2.2017

g=9.81;
format compact

%% Parameter des PKWs (Angepasst von Matthias)
%
mA=300;               % Masse Viertelauto
cA=30000;             % Federsteifigkeit Federung [N/m]
b=300;                % D�mpfung f�r Federung
l0A=0.2;                % Vorspannung Feder [m] (Abstand Radnabe zu Aufbau-Schwerpunkt)
mR=30;                 % Masse Rad
cR=120000 ;          % Federsteifigkeit Reifen
l0R=0.15;           % Vorspannung Reifen [m] (Abstand Radnabe zu Stra�e)

%% Matrizen f�r Variante 2 der L�sung
M=[mA 0; 0, mR];
M1=inv(M);
B=[b,-b;-b,b];
C=[cA,-cA;-cA,cR+cA]
G=[mA*g; mR*g]
F0=[cA*l0A; cR*l0R-cA*l0A]

%% Statisches Gleichgewicht (Angepasst von Matthias)
x0 = C^(-1)*(F0-G);

%% Signale
sweep.start=0.1;
sweep.end=15;
sweep.time=60;

kasten.hoehe=0.1/2;
kasten.frequenz= 2;     % rad/sec



%% "Kasten" mit Amplitude 20cm als Stra�enprofil
%
% $$v_y(t)=\frac{dy}{dt}=\frac{dy}{dx}\frac{dx}{dt}=\frac{dy}{dx}v(t)$$
%
v=30/3.6
dx=0.2;
x=[0,1,1+dx,2-dx,2,3];
dy=0.2;
y=dy*[0,0,1,1,0,0];
ddx=0.01;
x2=[x(1:2),x(2)+ddx,x(3),x(3)+ddx,x(4),x(4)+ddx,x(5),x(5)+ddx,x(6)];
m=dy/dx;
y2=[0,0,m,m,0,0,-m,-m,0,0];

%% Kontrolle: y(x)-Profil und Ableitung dy/dx
% figure
% plot(x,y,'r','linewidth',1.5);
% hold on
% plot(x2,y2,'b','linewidth',2.5);
% beautify('Vertikales Profil und Ableitung einer Stra�enschwelle','x/m','y/m bzw. dy/dx');
% legend('Vertikales y-Profil','Ableitung dy/dx');

%% Nun gegen die Zeit
% figure
% t=[0:0.1:1];
% plot(t,,'r','linewidth',1.5);
% beautify('Stra�enschwelle','t/sec','v*dy/dx');


%% Pulsprofil

% subplot(2,1,1)
% ax= gca;
% ax.XLabel.String='Zeit/sec';
% ax.YLabel.String='Geschwindigkeit/m/sec';
% fplot(ax,@(u)(-72*97.858*u.*exp(-72*u)+97.858*exp(-72*u))*1/3,[0,0.2])
% subplot(2,1,2)
% ax=gca;
% ax.YLabel.String='H�he/m';
% fplot(ax,@(u)97.858*u.*exp(-72*u)*1/3,[0,0.2])



